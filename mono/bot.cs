using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
//using HelloWorldOpen;
//using HelloWorldOpen.Algorithms;
//using HelloWorldOpen.Commands;
//using HelloWorldOpen.Messages;
using System.Timers;
using Newtonsoft.Json;

public class Bot
{
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];


        //Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        using (var client = new TcpClient(host, port))
        {
            var stream = client.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
            //new Bot(reader, writer, new JoinRace(botName, botKey, "germany", 1));
            //new Bot(reader, writer, new JoinRace(botName, botKey, "elaeintarha", 1));
            //new Bot(reader, writer, new JoinRace(botName, botKey, "keimola", 1));
            //var bot = new Bot(reader, writer, new JoinRace(botName, botKey, "keimola", 1));
            //var bot = new Bot(reader, writer, new JoinRace(botName, botKey, "germany", 1));
            //var bot = new Bot(reader, writer, new JoinRace(botName, botKey, "germany", 1));

        }

    }

    private StreamWriter writer;
    public int Message;
    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;

        //var endRace = false;
        var handler = new MessageHandler(join.name);
        send(join);


        Message = 0;
        while ((line = reader.ReadLine()) != null)
        {

            var msgType = Regex.Match(line, "(?<=msgType\":\")[\\s\\S]+?(?=\")").Value;
            switch (msgType)
            {
                case "carPositions":
                    Message++;
                    send(handler.HandleCarPositions(JsonConvert.DeserializeObject<CarPositionsMessage>(line)));
                    break;
                case "join":
                case "joinRace":
                    send(handler.HandleJoinAcknowledge());
                    break;
                case "yourCar":
                    send(handler.HandleYourCar(JsonConvert.DeserializeObject<YourCarMessage>(line)));
                    break;
                case "gameInit":
                    send(handler.HandleGameInit(JsonConvert.DeserializeObject<GameInitMessage>(line)));
                    break;
                case "gameEnd":
                    send(handler.HandleGameEnd(JsonConvert.DeserializeObject<GameEndMessage>(line)));
                    
                    break;
                case "gameStart":
                    send(handler.HandleGameStart());
                    break;
                case "tournamentEnd":
                    handler.HandleTournamentEnd();
                    break;
                case "crash":
                    send(handler.HandleCrash(JsonConvert.DeserializeObject<CrashMessage>(line)));
                    break;
                case "lapFinished":
                    //send(handler.HandleLapFinished());
                    Console.WriteLine("Lap Finished");
                    break;
                case "turboEnd":
                    handler.Session.TurboEnabled = false;
                    handler.Session.ThrottleMultiplier = 1.0;
                    send(new Ping());
                    break;
                case "turboAvailable":
                    send(handler.HandleTurboAvailable(JsonConvert.DeserializeObject<TurboAvailableMessage>(line)));
                    break;
                default:
                    //Console.WriteLine("UNKNOWN COMMAND: " + msgType);
                    send(new Ping());
                    break;
            }
        }
        Console.WriteLine("NO MESSAGE ANYMORE");
    }

    private void send(SendMsg msg)
    {
        if (msg != null)
            writer.WriteLine(msg.ToJson());
    }
}

public partial class MessageHandler
{
    public SendMsg ConstantSpeedAlgorithm()
    {
        SendMsg msg = new Throttle(1.0);
        msg = EmergencyBreaking(msg);
        msg = NormalFlow(msg);
        msg = PushTurbo(msg);
        msg = MakeSwitch(msg);

        if (msg as Turbo != null)
        {
            Session.TurboEnabled = true;
            Session.ThrottleMultiplier = Session.Turbos[0].turboFactor;
            Session.Turbos.RemoveAt(0);
            Session.SetCurrentComand(CommandType.Turbo);
            Console.WriteLine("ENTERED TURBO STATE at position {0}", Session.CurrentCarPosition.piecePosition.pieceIndex);
        }
        else if (msg as SwitchLane != null)
        {
            Session.SetCurrentComand(CommandType.Switch);
            Session.SetSwitch();
            Console.WriteLine("SWITCH, STAY TO LANE {0}, at position {1}", Session.CurrentCarPosition.piecePosition.lane.endLaneIndex,Session.CurrentCarPosition.piecePosition.pieceIndex);
        }
        else
        {
            Session.SetCurrentComand(CommandType.Throttle);
        }

        return msg;

    }

    public const int ROUNDS_TO_CRASH = 16;
    public const double CRASH_ANGLE = 60.0;
    private SendMsg EmergencyBreaking(SendMsg msg)
    {

        var previousDeltaAlfa1 = Session.CurrentAngle - Session.PreviousAngle1;
        var previousDeltaAlfa2 = Session.PreviousAngle1 - Session.PreviousAngle2;
        var previousDeltaAlfa3 = Session.PreviousAngle2 - Session.PreviousAngle3;
        var previousAcceleration1 = previousDeltaAlfa1 - previousDeltaAlfa2;
        var previousAcceleration2 = previousDeltaAlfa2 - previousDeltaAlfa3;
        var accelerationChange = previousAcceleration1 - previousAcceleration2;

        if (Math.Abs(Session.CurrentAngle + (previousDeltaAlfa1 + previousAcceleration1 + accelerationChange)*(ROUNDS_TO_CRASH + Session.Delay)) > CRASH_ANGLE)
        {
            Console.WriteLine("Emergency Break at piece {0}. CurrentAngle : {1}", Session.CurrentPieceIndex, Session.CurrentAngle);
            return new Throttle(0.0);
        }
            
        return msg;
    }

    private SendMsg MakeSwitch(SendMsg msg)
    {
        //var throttle = msg as Throttle;
        if (Session.SwitchesExist && Session.CanSwitch())// && throttle != null && Math.Abs(Session.GetPreviousCarThrottle(0) - throttle.value) < 2.0)//nie ma istotnej zmiany gazu
        {
            if (Session.NextPieceHasSwitch() 
                && Session.DelayedSpeed + Session.CurrentCarPosition.piecePosition.inPieceDistance > Session.GetPieceLength(Session.CurrentPieceIndex)) //jesli to ostatni moment na switcha
            {
                var nextSwitchIndex = Session.GetNextSwitchIndex(Session.CurrentPieceIndex, 2);
                var relativePositions = Session.GetRelativeCarPositions(nextSwitchIndex);
                var orientations = Session.GetBestLaneToSwitch();
                var timeToNextSwitch = Session.GetDistanceToPiece(Session.CurrentCarPosition, nextSwitchIndex)/Session.GetAvarageSpeed(Session.CurrentCarPosition, 5);
                string choosenOrientation = null;
                foreach (var orientation in orientations)
                {
                    var carTTNS = relativePositions.FirstOrDefault(x => x.Orientation == orientation);
                    if (carTTNS == null || timeToNextSwitch > carTTNS.Time + 2.0)
                    {
                        choosenOrientation = orientation;
                        break;
                    }
                }
                if (choosenOrientation == null) // nie ma tak latwo , musimy wybrac droga kompromisu
                {
                    var firstEmptyTrack = orientations.FirstOrDefault(x => relativePositions.All(y => y.Orientation != x));
                    if (firstEmptyTrack != null)
                    {
                        choosenOrientation = firstEmptyTrack;
                    }
                    else
                    {
                        var bestTime = double.MaxValue;
                        foreach (var position in relativePositions)
                        {
                            if (position.Time < bestTime)
                            {
                                bestTime = position.Time;
                                choosenOrientation = position.Orientation;
                            }
                        }
                    }
                }


                if (choosenOrientation == null || choosenOrientation == "Current")
                {
                    Console.WriteLine("NO SWITCH, STAY ON LANE {0}",Session.CurrentCarPosition.piecePosition.lane.endLaneIndex);
                    Session.SetSwitch(); //set that switch decision has been made
                }
                else
                {

                    msg = new SwitchLane(choosenOrientation);
                }
            }
            
        }
        return msg;
    }

    private SendMsg PushTurbo(SendMsg msg)
    {
        var throttle = (Throttle)msg;
        if (Session.Turbos.Any() && !Session.TurboEnabled && throttle.value.EqualsTo(1.0))
        {
            if (Session.CurrentPieceInfo == Session.TurboStartPiece ||
                (Session.CurrentPieceIndex + 1 == Session.GetPieceIndex(Session.TurboStartPiece)
                 &&
                 Session.CurrentCarPosition.piecePosition.inPieceDistance + Session.CurrentSpeed >
                 Session.GetPieceLength(Session.CurrentPieceIndex)))
            {
                Console.WriteLine("N.O.S. ENABLED at position {0}, distance {1}", Session.CurrentCarPosition.piecePosition.pieceIndex, Session.CurrentCarPosition.piecePosition.inPieceDistance);
                msg = new Turbo("N.O.S. ENABLED");
            }
            
        }

        return msg;
    }

    private SendMsg NormalFlow(SendMsg msg)
    {
        var throttle = (Throttle)msg;

        var nextTurnMaxSpeed = GetMaxPieceSpeed(Session.NextTurnRadius);
        var breakDistance = Session.GetBreakDistance(Session.DelayedSpeed, nextTurnMaxSpeed);

        var maxAllowedSpeedBeforeNextTurn = Session.NextTurnDistance - Session.DelayedDistance - breakDistance;
        var maxAllowedSpeedCurrentPiece = GetMaxPieceSpeed(Session.GetCurrentTurnRadius());

        var targetSpeed = Math.Min(maxAllowedSpeedBeforeNextTurn, maxAllowedSpeedCurrentPiece);

        var targetThrottle = Session.GetTargetThrottle(Session.DelayedSpeed, targetSpeed);


        return new Throttle(Math.Min(throttle.value, targetThrottle));
    }

    private const double FRICTION = 0.5; //0.47;

    private double GetMaxPieceSpeed(int radius)
    {
        if (radius == 0) return 100.0;
        if (radius >= 170) return 9.0;

        var maxSpeed = radius < 80 ? 
            Math.Sqrt(0.95 * radius * FRICTION)
            : radius >= 180 ? Math.Sqrt(radius*1.1 * FRICTION) : Math.Sqrt(radius * FRICTION);




        //TODO: move to emergency break
        var previousTurnDistance = Session.GetPreviousTurnDistance(Session.CurrentPieceInfo);
        var previousTurnAngle = Session.GetPreviousTurnPiece(Session.CurrentPieceInfo).angle;
        var breakDistance = Session.GetBreakDistance(Session.DelayedSpeed, maxSpeed);
        
        var carAngle = Session.CurrentAngle;

        if (Session.CurrentPieceInfo.angle * previousTurnAngle < 0 && (previousTurnDistance < breakDistance || (carAngle * Session.CurrentPieceInfo.angle < 0 && Math.Abs(carAngle) > 0.25)))
            maxSpeed *= 0.90;

        return maxSpeed;
    }
}

public partial class MessageHandler
{
    private readonly string _carName;
    public Session Session;
    public MessageHandler(string carName)
    {
        _carName = carName;
    }

    public SendMsg HandleGameInit(GameInitMessage message)
    {
        var myCar = message.data.race.cars.First(x => x.id.name == _carName);

        Session = Session == null ? new Session() : new Session(Session);
        Session.Game = message;
        Session.SetCarInfo(myCar.id.name, myCar.id.color);
        Session.History = new History();
        
        Session.SwitchesExist = Session.CheckIfSwitchesExist();
        Console.WriteLine("Race init " + myCar.dimensions);
        return new Throttle(1.0);
    }


    public SendMsg HandleYourCar(YourCarMessage deserializeObject)
    {
        return new Ping();
    }

    public SendMsg HandleGameEnd(GameEndMessage message)
    {
        var carBestLap = message.data.bestLaps.FirstOrDefault(x => x.car.color == Session.CarColor && x.car.name == Session.CarName);
        var result = message.data.results.First(x => x.car.color == Session.CarColor && x.car.name == Session.CarName);
        var position = Array.IndexOf(message.data.results,result) + 1;
        Console.WriteLine("RACE ENDED. BEST LAP: {0}. TIME {1}. POSITION {2}", carBestLap == null ? "NO" : carBestLap.result.millis.ToString(CultureInfo.InvariantCulture),result.result.millis,position);
        return new Ping();
    }

    public void HandleTournamentEnd()
    {
        //Console.WriteLine("TOURNAMENT END");
    }

    public SendMsg HandleCrash(CrashMessage deserializeObject)
    {
        Console.WriteLine("CRASH ON PIECE {0}", Session.CurrentCarPosition.piecePosition.pieceIndex);
        //Session.History.SetCrash(Session.CurrentPieceInfo);
        return new Ping();
    }

    public SendMsg TurboEnd()
    {
        //Console.WriteLine("TURBO END");
        return new Ping();
    }

    public SendMsg HandleGameStart()
    {
        //Console.WriteLine("RACE STARTS");
        return new Ping();
    }

    public SendMsg HandleJoinAcknowledge()
    {
        Console.WriteLine("CAR JOINED THE RACE");
        return new Ping();
    }

    public SendMsg HandleTurboAvailable(TurboAvailableMessage turboAvailableMessage)
    {
        //Console.WriteLine("TURBO::::: " + turboAvailableMessage.data.turboFactor);
        Session.Turbos.Add(turboAvailableMessage.data);
        return new Ping();
    }
}

public partial class MessageHandler
{
    public SendMsg HandleCarPositions(CarPositionsMessage message)
    {
        var t = new Stopwatch();
        t.Start();

        Session.AddCarPositions(message);
        Session.UpdateHistory(message.gameTick);
        if (Session.CarIsCrashed())
        {
            Session.TurboEnabled = false;
            Session.ThrottleMultiplier = 1.0;
            Console.WriteLine("CAR IS CRASHED");
        }


        try
        {
            var throttle = ConstantSpeedAlgorithm();
            //Console.WriteLine("{0};{1};{2};{3};{4};{5}",
            //    Session.Tick, throttle, Session.CurrentSpeed, Session.CurrentPieceInfo.radius, Session.CurrentPieceInfo.angle, Session.CurrentAngle);

            t.Stop();
            Session.SetThrottle(throttle);

            return throttle;
        }
        catch (Exception ex)
        {
            Console.WriteLine("STH WENT WRONG" + ex.Message);
            return new Throttle(0.5);
        }
    }
}









































public abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

public class JoinRace : SendMsg
{
    public BotId botId;
    public int carCount;
    public string trackName;

    public JoinRace(string name, string key, string trackName, int carCount)
    {
        botId = new BotId
        {
            name = name,
            key = key
        };
        this.trackName = trackName;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}

public class BotId
{
    public string name;
    public string key;
}


public class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

public class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
    
}

public class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }

    public override string ToString()
    {
        return string.Format("{0}", value);
    }
}

public class Turbo : SendMsg
{
    public string value;

    public Turbo(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }

    public override string ToString()
    {
        return value.ToString();
    }
}

public enum CommandType
{
    Throttle,
    Turbo,
    Switch
}



public class CarPositionsMessage
{
    public CommandType Command;
    public string msgType;
    public CarPosition[] data;
    public string gameId;
    public int gameTick;

    public double Speed;
    public double Distance;
    public double Throttle;
    public bool Crash;
    
    //public CarOrder CarOrder;
    public double TargetSpeed;
}

public class CarPosition
{
    public YourCar id;
    public double angle;
    public PiecePosition piecePosition;
    public int gameTick;

    public override string ToString()
    {
        return string.Format("{0}-{1,8}. A:{2:0.00,8},PP:{3:0.00,8}", id.name, id.color, angle, piecePosition);
    }
}

public class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public LanePosition lane;
    public int lap;

    public override string ToString()
    {
        return string.Format("{0,3}:{1:0.00,5}, LANE:{2,1}, LAP{3,1}", pieceIndex, inPieceDistance, lane, lap);
    }
}

public class LanePosition
{
    public int startLaneIndex;
    public int endLaneIndex;

    public override string ToString()
    {
        return endLaneIndex.ToString();
    }
}

public class CrashMessage
{
    public string msgType;
    public YourCar data;
    public string gameId;
    public int gameTick;
}


public class GameEndMessage
{
    public string msgType;
    public GameEnd data;
}

public class GameEnd
{
    public Result[] results;
    public BestLap[] bestLaps;
}

public class Result
{
    public YourCar car;
    public ResultInfo result;
}

public class ResultInfo
{
    public int laps;
    public int ticks;
    public int millis;
}

public class BestLap
{
    public YourCar car;
    public BestResult result;
}

public class BestResult
{
    public int lap;
    public int ticks;
    public int millis;
}


public class GameInitMessage
{
    public string msgType;
    public GameInit data;
    public string gameId;
}



public class Car
{
    public YourCar id;
    public Dimensions dimensions;

    public class Dimensions
    {
        public double length;
        public double width;
        public double guideFlagPosition;

        public override string ToString()
        {
            return String.Format("CAR DIMENSIONS(L/W/FG) : {0}/{1}/{2}", length, width, guideFlagPosition);
        }
    }


}

public class GameInit
{
    public Race race;
}

public class Race
{
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;
}

public class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;
}

public class StartingPoint
{
    public double angle;
    public Position position;
    public class Position
    {
        public double x;
        public double y;
    }
}

public class Track
{
    public string id;
    public string name;
    public Lane[] lanes;
    public Piece[] pieces;
    public StartingPoint startingPoint;
}

public class Piece
{
    public double length;
    public bool @switch;
    public int radius;
    public double angle;
}

public class Lane
{
    public int distanceFromCenter;
    public int index;

    public override string ToString()
    {
        return string.Format("{0}({1,5})", index, distanceFromCenter);
    }
}

public class JoinMessage
{
}

public class TurboAvailableMessage
{
    public string msgType;
    public TurboInfo data;
}

public class TurboInfo
{
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}

public class YourCarMessage
{
    public string msgType;
    public YourCar data;
}

public class YourCar
{
    public string name;
    public string color;
}


public class Session
{
    public int Tick = -1;
    public bool ParametersCounted = false;
    private double X1 = 0.2;
    private double X2 = 0.98;
    public int Delay = 1;

    public string CarName;
    public string CarColor;
    //public Chromosone BotChromosome;

    public History History;
    public GameInitMessage Game;
    public List<TurboInfo> Turbos = new List<TurboInfo>();

    public Session()
    {

    }

    public Session(Session session)
    {
        X1 = session.X1;
        X2 = session.X2;
        Delay = session.Delay;
        ParametersCounted = true;
    }

    public bool SwitchesExist;

    public double ThrottleMultiplier = 1.0;

    public void SetCarInfo(string carName, string carColor)
    {
        CarName = carName;
        CarColor = carColor;
    }

    public void AddCarPositions(CarPositionsMessage carPositions)
    {
        History.CarPositions.Add(carPositions);
    }

    private double GetCurrentCarSpeed()
    {
        var previousPosition = PreviousCarPosition;
        var currentPosition = CurrentCarPosition;
        if (currentPosition == null) return 0.0;
        if (previousPosition == null) return currentPosition.piecePosition.inPieceDistance;

        var speed = currentPosition.piecePosition.inPieceDistance - previousPosition.piecePosition.inPieceDistance;
        if (currentPosition.piecePosition.pieceIndex != previousPosition.piecePosition.pieceIndex)
            speed += GetPieceLength(previousPosition.piecePosition.pieceIndex);
        return speed;
    }

    public double GetPieceLength(int id)
    {
        return GetPieceLength(GetPieceInfo(id));
    }

    public double GetPieceLength(int id, int laneIndex)
    {
        return GetPieceLength(GetPieceInfo(id), laneIndex);
    }

    public int GetPieceIndex(Piece piece)
    {
        return Array.IndexOf(Game.data.race.track.pieces, piece);
    }
    public double GetPieceLength(Piece piece)
    {
        return GetPieceLength(piece, CurrentCarPosition.piecePosition.lane.endLaneIndex);
    }

    public double GetPieceLength(Piece piece, int laneIndex)
    {
        var laneDeviation = GetPieceLaneDeviation(piece,laneIndex);
        return piece.length > 0.0
                   ? piece.length
                   : 2 * Math.PI * (piece.radius - laneDeviation * piece.angle / Math.Abs(piece.angle)) * Math.Abs(piece.angle) / 360.0
        ;
    }

    public int GetLaneDistanceFromCenter(int id)
    {
        return Game.data.race.track.lanes.First(x => x.index == id).distanceFromCenter;
    }

    private int GetPreviousGameTick(int i)
    {
        if (History.CarPositions.Count <= 1 + i) return 0;
        return History.CarPositions[History.CarPositions.Count - 1 - i].gameTick;
    }

    private CarPosition GetCurrentCarPosition()
    {
        if (!History.CarPositions.Any()) return null;

        return GetCarPosition(History.CarPositions.Last());
    }

    public CarPosition GetCarPosition(CarPositionsMessage carPositionsMessage)
    {

        return carPositionsMessage.data.First(x => x.id.name == CarName && x.id.color == CarColor);
    }

    private CarPosition GetPreviuosCarPosition(int i)
    {
        if (History.CarPositions.Count <= 1 + i) return null;
        return History.CarPositions[History.CarPositions.Count - 1 - i].data.First(x => x.id.name == CarName && x.id.color == CarColor);
    }


    private double GetPreviousAngle(int i)
    {
        var carPosition = GetPreviuosCarPosition(i);
        return carPosition == null ? 0.0 : carPosition.angle;
    }

    public Piece GetPieceInfo(int piece)
    {
        return Game.data.race.track.pieces[piece];
    }

    public Car GetCarInfo()
    {
        return Game.data.race.cars.First(x => x.id.name == CarName && x.id.color == CarColor);
    }

    public double GetDistanceToNextStraight()
    {
        var currentPosition = CurrentCarPosition.piecePosition;
        var toNextStraight = 0.0;
        Piece p;
        var i = currentPosition.pieceIndex;
        do
        {
            p = GetPieceInfo(i);
            if (p.radius == 0) continue;
            toNextStraight += GetPieceLength(i);
            if (i == currentPosition.pieceIndex)
                toNextStraight -= currentPosition.inPieceDistance;

            i = (i + 1) % Game.data.race.track.pieces.Length;
        } while (p.radius > 0);
        return toNextStraight;
    }

    public Piece GetNextTurn()
    {
        var pi = CurrentCarPosition.piecePosition.pieceIndex;
        Piece piece;
        do
        {
            piece = GetPieceInfo(pi);
            pi = (pi + 1) % Game.data.race.track.pieces.Length;
        } while (piece.radius == 0);

        return piece;
    }

    /*Nowe funkcje*/

    public int GetNextTurnRadius()
    {
        var pieceInfo = GetNextTurnPiece();
        var r = pieceInfo.radius;
        //laneDeviation
        if (r > 0) r -= GetPieceLaneDeviation(pieceInfo) * (int)pieceInfo.angle / (int)Math.Abs(pieceInfo.angle);
        return r;
    }

    public double GetNextTurnAngle()
    {
        var i = GetNextTurnPieceIndex();
        var pAngle = GetPieceInfo(i).angle;
        var totalAngle = pAngle;
        do
        {
            i = (i + 1) % Game.data.race.track.pieces.Length;
            var p = GetPieceInfo(i);
            if (Math.Abs(p.angle - pAngle) > double.Epsilon) break;
            totalAngle += p.angle;
        } while (true);

        return totalAngle;
    }

    public int GetPieceLaneDeviation(Piece piece)
    {
        return GetPieceLaneDeviation(piece, CurrentCarPosition.piecePosition.lane.endLaneIndex);
    }

    public int GetPieceLaneDeviation(Piece piece, int index)
    {
        return Game.data.race.track.lanes[index].distanceFromCenter;
    }

    public double GetCurrentTurnDistance()
    {
        var currentTurnPieceIndex = GetCurrentCarPosition().piecePosition.pieceIndex;
        var currentPieceInfo = GetPieceInfo(currentTurnPieceIndex);
        if (currentPieceInfo.radius == 0) return 0; // nie zakret

        var angle = currentPieceInfo.angle;
        var totalLength = 0.0;

        var i = currentTurnPieceIndex;
        do
        {
            var p = GetPieceInfo(i);
            i = (i + 1) % Game.data.race.track.pieces.Length;
            if (p.angle != angle) break;
            totalLength += GetPieceLength(i);
            if (i == currentTurnPieceIndex)
            {
                totalLength -= GetCurrentCarPosition().piecePosition.inPieceDistance;
            }

        } while (true);

        return totalLength;
    }

    public int GetCurrentTurnRadius()
    {
        var currentTurnPieceIndex = GetCurrentCarPosition().piecePosition.pieceIndex;
        var r = GetPieceInfo(currentTurnPieceIndex).radius;
        var pieceInfo = GetPieceInfo(currentTurnPieceIndex);
        //laneDeviation
        if (r > 0) r -= GetPieceLaneDeviation(pieceInfo) * (int)pieceInfo.angle / (int)Math.Abs(pieceInfo.angle);
        return r;
    }

    public double GetCurrentTurnAngle()
    {
        var pp = GetCurrentCarPosition();
        var i = pp.piecePosition.pieceIndex;
        var pi = GetPieceInfo(i);

        if (pi.radius == 0) return 0.0; // nie zakret

        var angle = pi.angle;
        var totalAngle = angle;
        var j = 1;
        while (true)
        {
            var p1 = GetPieceInfo((i + j) % Game.data.race.track.pieces.Length);
            if (Math.Abs(p1.angle - angle) > double.Epsilon)
                break;
            totalAngle += angle;
            j++;
        }
        j = 1;
        while (true)
        {
            var p2 = GetPieceInfo((i - j + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length);
            if (Math.Abs(p2.angle - angle) > double.Epsilon)
                break;
            totalAngle += angle;
            j++;
        }
        return totalAngle;
    }



    public double GetNextTurnDistance()
    {
        var nextTurnPieceIndex = GetNextTurnPieceIndex();
        var toNextTurn = 0.0;
        var currentPosition = GetCurrentCarPosition();
        var i = currentPosition.piecePosition.pieceIndex;
        while (i != nextTurnPieceIndex)
        {
            toNextTurn += GetPieceLength(i);
            if (i == currentPosition.piecePosition.pieceIndex)
                toNextTurn -= currentPosition.piecePosition.inPieceDistance;

            i = (i + 1) % Game.data.race.track.pieces.Length;
        }
        return toNextTurn;
    }

    public double GetPreviousTurnDistance(Piece basePiece)
    {
        var previousTurnPiece = GetPreviousTurnPiece(basePiece);
        var previousIndex = GetPieceIndex(previousTurnPiece);
        var toPreviousTurn = 0.0;
        var pi = GetPieceIndex(basePiece);
        var i = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
        while (i != previousIndex)
        {
            toPreviousTurn += GetPieceLength(i);
            i = (i - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
        }
        return toPreviousTurn;
    }

    private int GetNextTurnPieceIndex()
    {
        return GetPieceIndex(GetNextTurnPiece());
    }

    public Piece GetPreviousTurnPiece(Piece basePiece)
    {
        var pi = GetPieceIndex(basePiece);


        Piece piece;

        //konczymy aktualny zakret
        do
        {
            pi = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
            piece = GetPieceInfo(pi);
        } while (piece.radius == basePiece.radius && piece.angle * basePiece.angle > 0);

        //szukamy nastepnego
        while (piece.radius == 0)
        {
            piece = GetPieceInfo(pi);
            pi = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
        }

        return piece;
    }

    private Piece GetNextTurnPiece()
    {
        var pi = GetCurrentCarPosition().piecePosition.pieceIndex;
        var pieceInfo = GetPieceInfo(pi);
        Piece piece;

        //konczymy aktualny zakret
        do
        {
            pi = (pi + 1) % Game.data.race.track.pieces.Length;
            piece = GetPieceInfo(pi);
        } while (piece.radius == pieceInfo.radius && piece.angle * pieceInfo.angle > 0);

        //szukamy nastepnego
        while (piece.radius == 0)
        {
            piece = GetPieceInfo(pi);
            pi = (pi + 1) % Game.data.race.track.pieces.Length;
        }

        return piece;
    }

    public bool IsTurn()
    {
        return GetCurrentTurnRadius() > 0;
    }

    //public bool IsBreaking()
    //{
    //    if (History.CarPositions.Count < 2) return false;
    //    return History.CarPositions[History.CarPositions.Count - 2].CarOrder == CarOrder.BREAK;
    //}

    public double GetTargetSpeed()
    {
        return History.CarPositions[History.CarPositions.Count - 2].TargetSpeed;
    }


    public double GetDistance(CarPositionsMessage startPosition, CarPositionsMessage endPosition)
    {
        return GetDistance(GetCarPosition(startPosition), GetCarPosition(endPosition));
    }

    public double GetDistance(CarPosition start, CarPosition end)
    {
        if (start.piecePosition.pieceIndex == end.piecePosition.pieceIndex)
            return end.piecePosition.inPieceDistance - start.piecePosition.inPieceDistance;

        var result = GetPieceLength(start.piecePosition.pieceIndex) - start.piecePosition.inPieceDistance + end.piecePosition.inPieceDistance;
        for (var i = start.piecePosition.pieceIndex + 1; i < end.piecePosition.pieceIndex; i++)
        {
            result += GetPieceLength(i);
        }
        return result;
    }

    public double GetDistanceToPiece(CarPosition start, int pieceIndex)
    {
        
        var result = GetPieceLength(start.piecePosition.pieceIndex) - start.piecePosition.inPieceDistance;
        for (var i = start.piecePosition.pieceIndex + 1; i < pieceIndex; i= (i+1) % Game.data.race.track.pieces.Length)
        {
            result += GetPieceLength(i);
        }
        return result;
    }

    //public double GetCarInPieceDistance()
    //{
    //    if (History.CarPositions.Count < 2) return 0.0;
    //    return GetDistance(History.CarPositions[History.CarPositions.Count - 2], History.CarPositions.Last());
    //}

    private double GetDelayedDistance()
    {
        var distance = 0.0;
        var speed = CurrentSpeed;
        for (var i = 0; i < Delay; i++)
        {
            speed = GetNextSpeed(speed, GetPreviousCarThrottle(Delay - i));
            distance += speed;
        }
        return distance;
    }

    private double GetDelayedSpeed()
    {
        var speed = CurrentSpeed;
        for (var i = Delay; i > 0; i--)
        {
            var throttle = GetPreviousCarThrottle(i);
            speed = GetNextSpeed(speed, throttle);
        }
        return speed;
    }

    public double GetNextSpeed(double currentSpeed, double throttle)
    {
        return 10 * X1 * throttle * ThrottleMultiplier + currentSpeed * X2;
    }

    public double DelayedDistance;
    public double NextTurnDistance;//dystans do nastepnego zakretu (nie biezemy pod uwage aktualnego)
    public int NextTurnRadius;
    public double NextTurnAngle;
    public CarPosition CurrentCarPosition;
    public Piece CurrentPieceInfo;
    public Piece NextTurnPiece;
    public CarPosition PreviousCarPosition;
    public double CurrentSpeed;
    public double DelayedSpeed;
    public double Previous1Speed;
    public double Previous2Speed;
    public double Previous3Speed;
    public double CurrentAngle;
    public double PreviousAngle1;
    public double PreviousAngle2;
    public double PreviousAngle3;
    public int CurrentPieceIndex;
    private int _nextDelayCheckTick = -100;
    public void UpdateHistory(int gameTick)
    {
        CurrentCarPosition = GetCurrentCarPosition();
        PreviousCarPosition = GetPreviuosCarPosition(1);
        CurrentPieceInfo = GetPieceInfo(CurrentCarPosition.piecePosition.pieceIndex);
        CurrentAngle = CurrentCarPosition.angle;
        CurrentPieceIndex = CurrentCarPosition.piecePosition.pieceIndex;

        History.CarPositions.Last().Speed = CurrentSpeed = GetCurrentCarSpeed();
        //History.CarPositions.Last().Distance = GetCarInPieceDistance();
        History.CarPositions.Last().gameTick = gameTick;

        Previous1Speed = GetPreviousCarSpeed(1);
        Previous2Speed = GetPreviousCarSpeed(2);
        Previous3Speed = GetPreviousCarSpeed(3);
        PreviousAngle1 = GetPreviousAngle(1);
        PreviousAngle2 = GetPreviousAngle(2);
        PreviousAngle3 = GetPreviousAngle(3);
        NextTurnPiece = GetNextTurn();
        NextTurnDistance = GetNextTurnDistance();
        NextTurnAngle = GetNextTurnAngle();
        NextTurnRadius = GetNextTurnRadius();
        DelayedDistance = GetDelayedDistance();
        DelayedSpeed = GetDelayedSpeed();
        if(TurboStartPiece == null) SetTurboPiece();
        Tick++;
        if (Tick > 8 && !ParametersCounted && GetPreviousGameTick(0) == GetPreviousGameTick(1) + 1 &&
                GetPreviousGameTick(1) == GetPreviousGameTick(2) + 1)
        {
            ParametersCounted = true;
            X2 = (CurrentSpeed - Previous1Speed) / (Previous1Speed - Previous2Speed);
            X1 = (CurrentSpeed - Previous1Speed * X2) / 10 * 1.0;

            //Console.WriteLine("Track type: {0} , {1}", X1, X2);    

        }

        var previousThrottle1 = GetPreviousCarThrottle(1);
        var previousThrottle2 = GetPreviousCarThrottle(2);
        var previousThrottle3 = GetPreviousCarThrottle(3);

        if (Tick > _nextDelayCheckTick && !previousThrottle1.EqualsTo(previousThrottle2) && !previousThrottle2.EqualsTo(previousThrottle3) && !previousThrottle1.EqualsTo(previousThrottle3))
        {
            var nvd1 = GetNextSpeed(Previous1Speed, GetPreviousCarThrottle(1));
            var nvd2 = GetNextSpeed(Previous1Speed, GetPreviousCarThrottle(2));
            var nvd3 = GetNextSpeed(Previous1Speed, GetPreviousCarThrottle(3));

            if (CurrentSpeed.EqualsTo(nvd1)) Delay = 0;
            else if (CurrentSpeed.EqualsTo(nvd2)) Delay = 1;
            else if (CurrentSpeed.EqualsTo(nvd3)) Delay = 2;
            else Delay = 1;
            _nextDelayCheckTick = Tick + 150;
        }
    }

    public double GetBreakDistance(double currentSpeed, double targetSpeed)
    {
        if (currentSpeed < targetSpeed) return 0.0;
        var distance = 0.0;
        var speed = currentSpeed;
        do
        {
            distance += speed;
            var throttle = GetTargetThrottle(speed, targetSpeed);
            speed = GetNextSpeed(speed, throttle);
        } while (!speed.EqualsTo(targetSpeed));
        return distance;
    }



    public double GetPreviousCarSpeed(int i)
    {
        if (History.CarPositions.Count < 1 + i) return 0.0;
        return History.CarPositions[History.CarPositions.Count - 1 - i].Speed;
    }

    public double GetPreviousCarThrottle(int i)
    {
        if (History.CarPositions.Count < 1 + i) return 0.0;
        return History.CarPositions[History.CarPositions.Count - 1 - i].Throttle;
    }

    public double GetTargetThrottle(double speed, double targetSpeed)
    {
        var targetThrottle = ((targetSpeed - speed * X2) / (10.0 * ThrottleMultiplier * X1));
        return Math.Max(Math.Min(targetThrottle, 1.0), 0.0);
    }

    public void SetTurboPiece()
    {
        Piece bestStartPiece = null;
        var bestDistance = 0.0;
        var currentDistance = 0.0;
        var currentStartPiece = Game.data.race.track.pieces[0];
        var noOfPiecesAnalyzed = 0;

        do
        {
            var straightRadius = Math.Max(180, Game.data.race.track.pieces.Where(x=>x.radius > 0).Min(x => x.radius));
            var currentPiece = Game.data.race.track.pieces[(noOfPiecesAnalyzed++) % Game.data.race.track.pieces.Length];
            if (currentPiece.radius == 0 || currentPiece.radius > straightRadius) //straight
            {
                currentDistance += GetPieceLength(currentPiece);
                if (currentStartPiece == null) currentStartPiece = currentPiece;
            }
            else //turn
            {
                if (currentDistance > bestDistance)
                {
                    bestDistance = currentDistance;
                    bestStartPiece = currentStartPiece;
                }
                currentDistance = 0.0;
                currentStartPiece = null;
            }

        } while (noOfPiecesAnalyzed <= 2 * Game.data.race.track.pieces.Length);

        TurboStartPiece = bestStartPiece;
    }

    public Piece TurboStartPiece;
    public bool TurboEnabled;

    public void SetThrottle(SendMsg throttle)
    {
        var th = throttle as Throttle;
        History.CarPositions.Last().Throttle = th != null ? th.value : GetPreviousCarThrottle(1);
    }

    public int LastSwtichIndex = 0;
    public int LastSwitchLap = -1;

    public void SetSwitch()
    {
        LastSwitchLap = CurrentCarPosition.piecePosition.lap;
        LastSwtichIndex = CurrentCarPosition.piecePosition.pieceIndex;
    }

    public bool CanSwitch()
    {
        if (CurrentCarPosition.piecePosition.lap != LastSwitchLap && LastSwtichIndex <= CurrentPieceIndex) //bylo pelne okrazenie bez switcha
            return true;

        if (CurrentCarPosition.piecePosition.lap == LastSwitchLap && LastSwtichIndex == CurrentPieceIndex) //switch byl na tym odcinku
            return false;

        var i = LastSwtichIndex + 1;
        do
        {
            if (GetPieceInfo(i).@switch) //there was a switch that previous switch command has been used for
                return true;
            i = (i+1)%Game.data.race.track.pieces.Length;
        } while (i <= CurrentPieceIndex);
        return false;
    }

    public List<string> GetBestLaneToSwitch() //index mniejszy => LEFT
    {
        var currentLaneIndex = CurrentCarPosition.piecePosition.lane.endLaneIndex;
        var switchIndex = GetNextSwitchIndex(CurrentCarPosition.piecePosition.pieceIndex, 1);
        var nextSwitchIndex = GetNextSwitchIndex(CurrentCarPosition.piecePosition.pieceIndex, 2);

        var left = double.MaxValue;
        var right = double.MaxValue;

        if (currentLaneIndex > 0)
            left = GetLaneDistance(switchIndex, nextSwitchIndex, currentLaneIndex - 1);
        if (currentLaneIndex < Game.data.race.track.lanes.Count()-1)
            right = GetLaneDistance(switchIndex, nextSwitchIndex, currentLaneIndex + 1);

        var current = GetLaneDistance(switchIndex, nextSwitchIndex, currentLaneIndex);
        var result = new List<string>();
        
        if (left < right && left < current)
        {
            result.Add("Left");
            result.Add("Current");
            if (right < double.MaxValue)
                result.Add("Right");
        }
        else if (right < left && right < current)
        {
            result.Add("Right");
            result.Add("Current");
            if (left < double.MaxValue)
                result.Add("Left");
            
        }
        else
        {
            result.Add("Current");
            if (right < double.MaxValue) result.Add("Right");
            if (left < double.MaxValue) result.Add("Left");
        }
        return result;

    }

    private double GetLaneDistance(int startSwitch, int endSwitch, int lineId)
    {
        var i = (startSwitch + 1) % Game.data.race.track.pieces.Length;
        var distance = 0.0;
        while (i != endSwitch)
        {
            distance += GetPieceLength(i, lineId);
            i = (i + 1) % Game.data.race.track.pieces.Length;
        }
        return distance;
    }

    public int GetNextSwitchIndex(int currentIndex, int noOfSwitch)
    {
        var i = (currentIndex + 1) % Game.data.race.track.pieces.Length;
        do
        {
            if (GetPieceInfo(i).@switch)
            {
                noOfSwitch--;
                if (noOfSwitch == 0) return i;
            }
            i = (i + 1) % Game.data.race.track.pieces.Length
                ;
        } while (i != currentIndex);

        return currentIndex;
    }

    public bool CheckIfSwitchesExist()
    {
        return Game.data.race.track.pieces.FirstOrDefault(x => x.@switch) != null;
    }

    public bool CarIsCrashed()
    {
        return Tick > 10 && CurrentSpeed.Equals(0.0);
    }

    public void SetCurrentComand(CommandType commandType)
    {
        if(History.CarPositions.Any())
            History.CarPositions.Last().Command = commandType;
    }

    public bool NextPieceHasSwitch()
    {
        return Game.data.race.track.pieces[((CurrentPieceIndex + 1)%Game.data.race.track.pieces.Length)].@switch;
    }


    public List<TimeToNextSwitch> GetRelativeCarPositions(int maxPieceIndex)
    {
        var result = new List<RelativeCarPosition>();
        if (!History.CarPositions.Any()) return new List<TimeToNextSwitch>();

        var maxDistanceToMaxPiece = GetDistanceToPiece(CurrentCarPosition, maxPieceIndex);
        foreach (var carPosition in History.CarPositions.Last().data)
        {
            if(carPosition == CurrentCarPosition) continue;

            var distanceToNextSwitch = GetDistanceToPiece(carPosition, maxPieceIndex);
            if (distanceToNextSwitch < maxDistanceToMaxPiece || Math.Abs(carPosition.piecePosition.lane.endLaneIndex - CurrentCarPosition.piecePosition.lane.endLaneIndex)>1) //car is between me and next switch and is on relevant track for me
            {
                var carDistance = GetDistance(CurrentCarPosition, carPosition);
                var avarageSpeed = GetAvarageSpeed(carPosition, 5);
                var timeToNextSwitch = distanceToNextSwitch/avarageSpeed;
                result.Add(new RelativeCarPosition(carDistance, avarageSpeed, carPosition, distanceToNextSwitch,timeToNextSwitch,CurrentCarPosition.piecePosition.lane.endLaneIndex,carPosition.piecePosition.lane.endLaneIndex));    
            }
        }
        return (from r in result
                group r by r.RelativeLane
                into g
                select new TimeToNextSwitch
                    {
                        Orientation = g.Key,
                        Time = g.Max(x => x.TimeToNextSwitch)
                    }).ToList();
    }

    public double GetAvarageSpeed(CarPosition currentPosition, int numberOfSamples)
    {
        var firstSampleIndex = Math.Max(History.CarPositions.Count - numberOfSamples - 1,0);
        double noOfSamples = History.CarPositions.Count - 1 - firstSampleIndex;

        var firstCarPosition = History.CarPositions[firstSampleIndex].data.First(x=>x.id.name == currentPosition.id.name);
        return GetDistance(firstCarPosition, currentPosition)/noOfSamples;
    }
}

public class TimeToNextSwitch
{
    public string Orientation;
    public double Time;
}

public class RelativeCarPosition
{
    public RelativeCarPosition(double distanceToCar, double avarageSpeed, CarPosition carPosition, double distanceToNextSwitch, double timeToNextSwith, int myLaneId, int carLaneId)
    {
        DistanceToCar = distanceToCar;
        AvarageSpeed = avarageSpeed;
        CarPosition = carPosition;
        DistanceToNextSwitch = distanceToNextSwitch;
        TimeToNextSwitch = timeToNextSwith;
        _myLaneId = myLaneId;
        _carLaneId = carLaneId;
    }
    public double DistanceToCar;
    public double DistanceToNextSwitch;
    public double AvarageSpeed;
    public CarPosition CarPosition;
    public double TimeToNextSwitch;
    private readonly int _myLaneId;
    private readonly int _carLaneId;
    public string RelativeLane {
        get { if (_myLaneId > _carLaneId) return "Left";
        return _myLaneId < _carLaneId ? "Right" : "Current";
        }
    }

}

public static class DoubleHelper
{
    public static bool EqualsTo(this double val1, double val2)
    {
        return Math.Abs(val1 - val2) < 0.0001;
    }
}


public class History
{
    public List<CarPositionsMessage> CarPositions = new List<CarPositionsMessage>();

    public void SetSpeedHistory(double currentSpeed)
    {
        CarPositions.Last().Speed = currentSpeed;
    }


    public void SetTargetSpeed(double targetSpeed)
    {
        CarPositions.Last().TargetSpeed = targetSpeed;
    }




}