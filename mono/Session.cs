﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelloWorldOpen.Algorithms;
using HelloWorldOpen.Messages;

namespace HelloWorldOpen
{
    public class Session
    {
        public int Tick = -1;
        private double X1 = 0.2;
        private double X2 = 1;
        public int Delay = 1; 
        
        public string CarName;
        public string CarColor;
        public Chromosone BotChromosome;

        public History History; 
        public GameInitMessage Game;
        public TurboInfo Turbo;

        public void SetCarInfo(string carName, string carColor)
        {
            CarName = carName;
            CarColor = carColor;
        }

        public void AddCarPositions(CarPositionsMessage carPositions)
        {
            History.CarPositions.Add(carPositions);
        }

        private double GetCurrentCarSpeed()
        {
            var previousPosition = GetPreviuosCarPosition(1);
            var currentPosition = GetCurrentCarPosition();
            if (currentPosition == null) return 0.0;
            if (previousPosition == null) return currentPosition.piecePosition.inPieceDistance;

            var speed = currentPosition.piecePosition.inPieceDistance - previousPosition.piecePosition.inPieceDistance;
            if (currentPosition.piecePosition.pieceIndex != previousPosition.piecePosition.pieceIndex)
                speed += GetPieceLength(previousPosition.piecePosition.pieceIndex);
            return speed;
        }

        public double GetPieceLength(int id)
        {
            return GetPieceLength(GetPieceInfo(id));
        }

        public int GetPieceIndex(Piece piece)
        {
            return Array.IndexOf(Game.data.race.track.pieces, piece);
        }
        public double GetPieceLength(Piece piece)
        {
            var laneDeviation = GetPieceLaneDeviation(piece);
            return piece.length > 0.0
                       ? piece.length
                       : 2 * Math.PI * (piece.radius - laneDeviation * piece.angle / Math.Abs(piece.angle)) * Math.Abs(piece.angle) / 360.0
            ;
        }

        public int GetLaneDistanceFromCenter(int id)
        {
            return Game.data.race.track.lanes.First(x => x.index == id).distanceFromCenter;
        }

        private CarPosition GetCurrentCarPosition()
        {
            if (!History.CarPositions.Any()) return null;
            
            return GetCarPosition(History.CarPositions.Last());
        }

        public CarPosition GetCarPosition(CarPositionsMessage carPositionsMessage)
        {
            
            return carPositionsMessage.data.First(x => x.id.name == CarName && x.id.color == CarColor);
        }

        private CarPosition GetPreviuosCarPosition(int i)
        {
            if (History.CarPositions.Count <= 1 + i) return null;
            return History.CarPositions[History.CarPositions.Count - 1 - i].data.First(x => x.id.name == CarName && x.id.color == CarColor);
        }


        private double GetPreviousAngle(int i)
        {
            var carPosition = GetPreviuosCarPosition(i);
            return carPosition == null ? 0.0 : carPosition.angle;
        }

        public Piece GetPieceInfo(int piece)
        {
            return Game.data.race.track.pieces[piece];
        }

        public Car GetCarInfo()
        {
            return Game.data.race.cars.First(x => x.id.name == CarName && x.id.color == CarColor);
        }


        //public double GetDistanceToNextTurn()
        //{
        //    var currentPosition = GetCurrentCarPosition().piecePosition;
        //    var toNextTurn = 0.0;
        //    Piece p;
        //    var i = currentPosition.pieceIndex;
        //    do
        //    {
        //        p = GetPieceInfo(i);
        //        if (p.radius != 0) continue;
        //        toNextTurn += GetPieceLength(i, currentPosition.lane.endLaneIndex);
        //        if (i == currentPosition.pieceIndex)
        //            toNextTurn -= currentPosition.inPieceDistance;

        //        i = (i + 1) % Game.data.race.track.pieces.Length;
        //    } while (p.radius == 0);
        //    return toNextTurn;
        //}

        public double GetDistanceToNextStraight()
        {
            var currentPosition = GetCurrentCarPosition().piecePosition;
            var toNextStraight = 0.0;
            Piece p;
            var i = currentPosition.pieceIndex;
            do
            {
                p = GetPieceInfo(i);
                if (p.radius == 0) continue;
                toNextStraight += GetPieceLength(i);
                if (i == currentPosition.pieceIndex)
                    toNextStraight -= currentPosition.inPieceDistance;

                i = (i + 1) % Game.data.race.track.pieces.Length;
            } while (p.radius > 0);
            return toNextStraight;
        }

        public Piece GetNextTurn()
        {
            var pi = GetCurrentCarPosition().piecePosition.pieceIndex;
            Piece piece;
            do
            {
                piece = GetPieceInfo(pi);
                pi = (pi + 1) % Game.data.race.track.pieces.Length;
            } while (piece.radius == 0 || piece.radius >= 180);

            return piece;
        }

        /*Nowe funkcje*/

        public int GetNextTurnRadius()
        {
            var pieceInfo = GetNextTurnPiece();
            var r = pieceInfo.radius;
            //laneDeviation
            if (r > 0) r -= GetPieceLaneDeviation(pieceInfo) * (int)pieceInfo.angle / (int)Math.Abs(pieceInfo.angle);
            return r >= 180 ? 0 : r;
        }

        public double GetNextTurnAngle()
        {
            var i = GetNextTurnPieceIndex();
            var pAngle = GetPieceInfo(i).angle;
            var totalAngle = pAngle;
            do
            {
                i = (i + 1) % Game.data.race.track.pieces.Length;
                var p = GetPieceInfo(i);
                if(Math.Abs(p.angle - pAngle) > double.Epsilon) break;
                totalAngle += p.angle;
            } while (true);

            return totalAngle;
        }

        public int GetPieceLaneDeviation(Piece piece)
        {
            return Game.data.race.track.lanes[0].distanceFromCenter;
            //throw new NotImplementedException();
            //var currentCarPosition = GetCurrentCarPosition();
            //var currentPieceIndex = currentCarPosition.piecePosition.pieceIndex;
            //var currentTrackId = currentCarPosition.piecePosition.lane.endLaneIndex;


            //if()
            // obliczyc na poczatku
        }

        public double GetCurrentTurnDistance()
        {
            var currentTurnPieceIndex = GetCurrentCarPosition().piecePosition.pieceIndex;
            var currentPieceInfo = GetPieceInfo(currentTurnPieceIndex);
            if (currentPieceInfo.radius == 0 || currentPieceInfo.radius > 200) return 0; // nie zakret

            var angle = currentPieceInfo.angle;
            var totalLength = 0.0;

            var i = currentTurnPieceIndex;
            do
            {
                var p = GetPieceInfo(i);
                i = (i + 1) % Game.data.race.track.pieces.Length;
                if (p.angle != angle) break;
                totalLength += GetPieceLength(i);
                if (i == currentTurnPieceIndex)
                {
                    totalLength -= GetCurrentCarPosition().piecePosition.inPieceDistance;
                }

            } while (true);

            return totalLength;
        }

        public int GetCurrentTurnRadius()
        {
            var currentTurnPieceIndex = GetCurrentCarPosition().piecePosition.pieceIndex;
            var r = GetPieceInfo(currentTurnPieceIndex).radius;
            var pieceInfo = GetPieceInfo(currentTurnPieceIndex);
            //laneDeviation
            if (r > 0) r -= GetPieceLaneDeviation(pieceInfo) * (int)pieceInfo.angle / (int)Math.Abs(pieceInfo.angle);
            return r;
            //return r >= 180 ? 0 : r;
        }

        public double GetCurrentTurnAngle()
        {
            var pp = GetCurrentCarPosition();
            var i = pp.piecePosition.pieceIndex;
            var pi = GetPieceInfo(i);

            if (pi.radius == 0 || pi.radius >= 200) return 0.0; // nie zakret

            var angle = pi.angle;
            var totalAngle = angle;
            var j = 1;
            while (true)
            {
                var p1 = GetPieceInfo(i + j%Game.data.race.track.pieces.Length);
                if (Math.Abs(p1.angle - angle) > double.Epsilon)
                    break;
                totalAngle += angle;
                j++;
            }
            j = 1;
            while (true)
            {
                var p2 = GetPieceInfo(i - j + Game.data.race.track.pieces.Length % Game.data.race.track.pieces.Length);
                if (Math.Abs(p2.angle - angle) > double.Epsilon)
                    break;
                totalAngle += angle;
                j++;
            }
            return totalAngle;
        }



        public double GetNextTurnDistance()
        {
            var nextTurnPieceIndex = GetNextTurnPieceIndex();
            var toNextTurn = 0.0;
            var currentPosition = GetCurrentCarPosition();
            var i = currentPosition.piecePosition.pieceIndex;
            while (i != nextTurnPieceIndex)
            {
                toNextTurn += GetPieceLength(i);
                if (i == currentPosition.piecePosition.pieceIndex)
                    toNextTurn -= currentPosition.piecePosition.inPieceDistance;

                i = (i + 1) % Game.data.race.track.pieces.Length;
            }
            return toNextTurn;
        }

        public double GetPreviousTurnDistance(Piece basePiece)
        {
            var previousTurnPiece = GetPreviousTurnPiece(basePiece);
            var previousIndex = GetPieceIndex(previousTurnPiece);
            var toPreviousTurn = 0.0;
            var pi = GetPieceIndex(basePiece);
            var i = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
            while (i != previousIndex)
            {
                toPreviousTurn += GetPieceLength(i);
                i = (i - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
            }
            return toPreviousTurn;
        }

        private int GetNextTurnPieceIndex()
        {
            return GetPieceIndex(GetNextTurnPiece());
        }

        public Piece GetPreviousTurnPiece(Piece basePiece)
        {
            var pi = GetPieceIndex(basePiece);
            

            Piece piece;

            //konczymy aktualny zakret
            do
            {
                pi = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
                piece = GetPieceInfo(pi);
            } while (piece.radius == basePiece.radius && piece.angle * basePiece.angle > 0);

            //szukamy nastepnego
            while (piece.radius == 0 || piece.radius >= 200)
            {
                piece = GetPieceInfo(pi);
                pi = (pi - 1 + Game.data.race.track.pieces.Length) % Game.data.race.track.pieces.Length;
            }

            return piece;
        }

        private Piece GetNextTurnPiece()
        {
            var pi = GetCurrentCarPosition().piecePosition.pieceIndex;
            var pieceInfo = GetPieceInfo(pi);
            Piece piece;

            //konczymy aktualny zakret
            do
            {
                pi = (pi + 1) % Game.data.race.track.pieces.Length;
                piece = GetPieceInfo(pi);
            } while (piece.radius == pieceInfo.radius && piece.angle * pieceInfo.angle > 0);

            //szukamy nastepnego
            while (piece.radius == 0 || piece.radius >= 200)
            {
                piece = GetPieceInfo(pi);
                pi = (pi + 1) % Game.data.race.track.pieces.Length;
            }

            return piece;
        }

        public bool IsTurn()
        {
            return GetCurrentTurnRadius() > 0;
        }

        public bool IsBreaking()
        {
            if (History.CarPositions.Count < 2) return false;
            return History.CarPositions[History.CarPositions.Count-2].CarOrder == CarOrder.BREAK;
        }

        public double GetTargetSpeed()
        {
            return History.CarPositions[History.CarPositions.Count-2].TargetSpeed;
        }


        public double GetDistance(CarPositionsMessage startPosition, CarPositionsMessage endPosition)
        {
            var start = GetCarPosition(startPosition);
            var end = GetCarPosition(endPosition);
            if (start.piecePosition.pieceIndex == end.piecePosition.pieceIndex)
                return end.piecePosition.inPieceDistance - start.piecePosition.inPieceDistance;

            var result = GetPieceLength(start.piecePosition.pieceIndex) - start.piecePosition.inPieceDistance + end.piecePosition.inPieceDistance;
            for (var i = start.piecePosition.pieceIndex + 1; i < end.piecePosition.pieceIndex; i++)
            {
                result += GetPieceLength(i);
            }
            return result;
        }

        public double GetCarInPieceDistance()
        {
            if (History.CarPositions.Count < 2) return 0.0;
            return GetDistance(History.CarPositions[History.CarPositions.Count - 2], History.CarPositions.Last());
        }

        private double GetDelayedDistance()
        {
            var distance = 0.0;
            var speed = CurrentSpeed;
            for (var i = 0; i < Delay; i++)
            {
                speed = GetNextSpeed(speed, GetPreviousCarThrottle(Delay-i));
                distance += speed;
            }
            return distance;
        }

        private double GetDelayedSpeed()
        {
            var speed = CurrentSpeed;
            for (var i = Delay; i > 0; i--)
            {
                var throttle = GetPreviousCarThrottle(i);
                speed = GetNextSpeed(speed, throttle);
            }
            return speed;
        }

        public double GetNextSpeed(double currentSpeed, double throttle)
        {
            return 10 * X1 * throttle + currentSpeed * X2;
        }

        public double DelayedDistance;
        public double NextTurnDistance;//dystans do nastepnego zakretu (nie biezemy pod uwage aktualnego)
        public int NextTurnRadius;
        public double NextTurnAngle;
        public CarPosition CurrentCarPosition;
        public Piece CurrentPieceInfo;
        public Piece NextTurnPiece;
        public CarPosition PreviousCarPosition;
        public double CurrentSpeed;
        public double DelayedSpeed;
        public double Previous1Speed;
        public double Previous2Speed;
        public double Previous3Speed;
        public double CurrentAngle;
        public double PreviousAngle1;
        public double PreviousAngle2;
        public double PreviousAngle3;
        public int CurrentPieceIndex;
        private int _nextDelayCheckTick = -100;
        public void UpdateHistory()
        {
            
            History.CarPositions.Last().Speed = CurrentSpeed = GetCurrentCarSpeed();
            History.CarPositions.Last().Distance = GetCarInPieceDistance();
            Previous1Speed = GetPreviousCarSpeed(1);
            Previous2Speed = GetPreviousCarSpeed(2);
            Previous3Speed = GetPreviousCarSpeed(3);
            CurrentCarPosition = GetCurrentCarPosition();
            PreviousCarPosition = GetPreviuosCarPosition(1);
            CurrentPieceInfo = GetPieceInfo(CurrentCarPosition.piecePosition.pieceIndex);
            CurrentAngle = CurrentCarPosition.angle;
            PreviousAngle1 = GetPreviousAngle(1);
            PreviousAngle2 = GetPreviousAngle(2);
            PreviousAngle3 = GetPreviousAngle(3);
            CurrentPieceIndex = CurrentCarPosition.piecePosition.pieceIndex;
            NextTurnPiece = GetNextTurn();
            NextTurnDistance = GetNextTurnDistance();
            NextTurnAngle = GetNextTurnAngle();
            NextTurnRadius = GetNextTurnRadius();
            DelayedDistance = GetDelayedDistance();
            DelayedSpeed = GetDelayedSpeed();

            Tick++;
            if (Tick == 6)
            {
                X2 = (CurrentSpeed - Previous1Speed) / (Previous1Speed - Previous2Speed);
                X1 = (CurrentSpeed - Previous1Speed * X2) / 10 * 1.0;
                Console.WriteLine("Track type: {0} , {1}", X1, X2);
            }

            var previousThrottle1 = GetPreviousCarThrottle(1);
            var previousThrottle2 = GetPreviousCarThrottle(2);
            var previousThrottle3 = GetPreviousCarThrottle(3);

            if(Tick > _nextDelayCheckTick && !previousThrottle1.Equals(previousThrottle2) && !previousThrottle2.Equals(previousThrottle3) && !previousThrottle1.Equals(previousThrottle3))
            {
                var nvd1 = GetNextSpeed(Previous1Speed,GetPreviousCarThrottle(1));
                var nvd2 = GetNextSpeed(Previous1Speed, GetPreviousCarThrottle(2));
                var nvd3 = GetNextSpeed(Previous1Speed, GetPreviousCarThrottle(3));

                if (CurrentSpeed.Equals(nvd1)) Delay = 0;
                else if (CurrentSpeed.Equals(nvd2)) Delay = 1;
                else if (CurrentSpeed.Equals(nvd3)) Delay = 2;
                else Delay = 1;
                _nextDelayCheckTick = Tick + 150;
            }
        }

        public double GetBreakDistance(double currentSpeed, double targetSpeed)
        {
            var distance = 0.0;
            var speed = currentSpeed;
            do
            {
                distance += speed;
                var throttle = GetTargetThrottle(speed, targetSpeed);
                speed = GetNextSpeed(speed, throttle);
            } while (!speed.Equals(targetSpeed));
            return distance;
        }

        

        public double GetPreviousCarSpeed(int i)
        {
            if(History.CarPositions.Count < 1 + i) return 0.0;
            return History.CarPositions[History.CarPositions.Count - 1 - i].Speed;
        }

        public double GetPreviousCarThrottle(int i)
        {
            if (History.CarPositions.Count < 1 + i) return 0.0;
            return History.CarPositions[History.CarPositions.Count - 1 - i].Throttle;
        }

        public double GetTargetThrottle(double speed, double targetSpeed)
        {
            var targetThrottle = ((targetSpeed - speed * X2) / (10.0 * X1));
            return Math.Max(Math.Min(targetThrottle, 1.0), 0.0);
        }
    }

    public static class DoubleHelper
    {
        public static bool Equals(this double val1, double val2)
        {
            return Math.Abs(val1 - val2) < 0.0001;
        }
    }
}
