﻿using System;

namespace HelloWorldOpen
{
    public class GameInitMessage
    {
        public string msgType;
        public GameInit data;
        public string gameId;
    }



    public class Car
    {
        public YourCar id;
        public Dimensions dimensions;

        public class Dimensions
        {
            public double length;
            public double width;
            public double guideFlagPosition;

            public override string ToString()
            {
                return String.Format("CAR DIMENSIONS(L/W/FG) : {0}/{1}/{2}", length, width, guideFlagPosition);
            }
        }

        
    }

    public class GameInit
    {
        public Race race;
    }

    public class Race
    {
        public Track track;
        public Car[] cars;
        public RaceSession raceSession;
    }

    public class RaceSession
    {
        public int laps;
        public int maxLapTimeMs;
        public bool quickRace;
    }

    public class StartingPoint
    {
        public double angle;
        public Position position;
        public class Position
        {
            public double x;
            public double y;
        }
    }

    public class Track
    {
        public string id;
        public string name;
        public Lane[] lanes;
        public Piece[] pieces;
        public StartingPoint startingPoint;
    }

    public class Piece
    {
        public double length;
        public bool @switch;
        public int radius;
        public double angle;
    }

    public class Lane
    {
        public int distanceFromCenter;
        public int index;

        public override string ToString()
        {
            return string.Format("{0}({1,5})",index ,distanceFromCenter);
        }
    }
}