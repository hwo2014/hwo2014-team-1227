﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldOpen.Messages
{
    public class CrashMessage
    {
        public string msgType;
        public YourCar data;
        public string gameId;
        public int gameTick;
    }
}
