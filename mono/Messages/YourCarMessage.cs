﻿using Models;

namespace HelloWorldOpen
{
    public class YourCarMessage
    {
        public string msgType;
        public YourCar data;
    }

    public class YourCar
    {
        public string name;
        public string color;
    }

    
}
