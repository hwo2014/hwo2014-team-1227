﻿using HelloWorldOpen.Algorithms;

namespace HelloWorldOpen.Messages
{
    public class CarPositionsMessage
    {
        public string msgType;
        public CarPosition[] data;
        public string gameId;
        public int gameTick;

        public double Speed;
        public double Distance;
        public double Throttle;
        public CarOrder CarOrder;
        public double TargetSpeed;
    }

    public class CarPosition
    {
        public YourCar id;
        public double angle;
        public PiecePosition piecePosition;
        

        public override string ToString()
        {
            return string.Format("{0}-{1,8}. A:{2:0.00,8},PP:{3:0.00,8}", id.name, id.color, angle, piecePosition);
        }
    }

    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public LanePosition lane;
        public int lap;

        public override string ToString()
        {
            return string.Format("{0,3}:{1:0.00,5}, LANE:{2,1}, LAP{3,1}", pieceIndex, inPieceDistance, lane, lap);
        }
    }

    public class LanePosition
    {
        public int startLaneIndex;
        public int endLaneIndex;

        public override string ToString()
        {
            return endLaneIndex.ToString();
        }
    }
}
