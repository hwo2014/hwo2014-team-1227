﻿namespace HelloWorldOpen.Messages
{
    public class GameEndMessage
    {
        public string msgType;
        public GameEnd data;
    }

    public class GameEnd
    {
        public Result[] results;
        public BestLap[] bestLaps;
    }

    public class Result
    {
        public YourCar car;
        public ResultInfo result;
    }

    public class ResultInfo
    {
        public int laps;
        public int ticks;
        public int millis;
    }

    public class BestLap
    {
        public YourCar car;
        public BestResult result;
    }

    public class BestResult
    {
        public int lap;
        public int ticks;
        public int millis;
    }
}
