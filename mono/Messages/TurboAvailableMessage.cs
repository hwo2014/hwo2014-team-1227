﻿namespace HelloWorldOpen.Messages
{
    public class TurboAvailableMessage
    {
        public string msgType;
        public TurboInfo data;
    }

    public class TurboInfo
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }

}
