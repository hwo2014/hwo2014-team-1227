﻿using System;
using HelloWorldOpen.Commands;

namespace HelloWorldOpen.Algorithms
{
    public partial class MessageHandler
    {
        private double t = 0.47;

        public SendMsg ConstantSpeedAlgorithmOld()
        {
            var nextTurnDistance = Session.GetNextTurnDistance();
            var nextTurnRadius = Session.GetNextTurnRadius();
            var nextTurnAngle = Session.GetNextTurnAngle();

            var nextTurnMaxSpeed = GetMaxSpeedOld(Session.GetNextTurn(),nextTurnRadius, nextTurnAngle);
            var breakDistance = GetBreakDistanceOld(nextTurnMaxSpeed,Session.Previous1Speed, Session.CurrentSpeed);

            //hamulec
            //zakret
            //turbo
            //switch
            //gaz
            



            if (Session.IsBreaking())
            {
                //accelerating = false;
                if (Session.GetTargetSpeed() < 2 * Session.CurrentSpeed - Session.Previous1Speed) //still need to break
                {
                    Session.History.SetOrder(CarOrder.BREAK);
                    Session.History.SetTargetSpeed(Session.GetTargetSpeed());

                    if (nextTurnDistance > 0.0 && nextTurnDistance > breakDistance) //breaking too fast
                    {
                        return new Throttle(breakDistance / nextTurnDistance * Session.GetTargetSpeed());
                    }
                    
                    return new Throttle(0.0); //breaking normally
                }
            }

            if (Session.CurrentSpeed > nextTurnMaxSpeed && breakDistance + Session.CurrentSpeed > nextTurnDistance)
            {
                //accelerating = false;
                Session.History.SetOrder(CarOrder.BREAK);
                Session.History.SetTargetSpeed(nextTurnMaxSpeed);
                return new Throttle(0.0);
            }

            if (Session.IsTurn())
            {
                //var currentTurnDistance = Session.GetCurrentTurnDistance(); // sprawdzic czy dobrze liczy 
                var currentTurnRadius = Session.GetCurrentTurnRadius();
                var currentTurnAngle = Session.GetCurrentTurnAngle();
                var currentMaxSpeed = GetMaxPieceSpeed(Session.CurrentPieceInfo);
                
                if (Session.CurrentSpeed - currentMaxSpeed > 0.1)
                    return new Throttle(0.5);

                if (Math.Abs(Session.CurrentAngle) > 30.0 && Math.Abs(Session.CurrentAngle) < Math.Abs(Session.PreviousCarPosition.angle))
                {
                    Session.History.SetOrder(CarOrder.SPEED);
                    return new Throttle(1.0);
                }
                //if (Math.Abs(currentAngle) > 20.0)
                //{
                //    if (!accelerating)
                //    {
                //        accelerating = true;
                //        currentSp = currentMaxSpeed;
                //    }
                //    if (accelerating)
                //    {
                //        currentSp = Math.Min(currentSp + (10.0 - currentMaxSpeed) / 10, 10.0);
                //    }
                //    //if (currentSp >= 10.0) accelerating = false;

                //    Session.History.SetOrder(CarOrder.SPEED);
                //    return new Throttle(currentSp);
                //}

                Session.History.SetOrder(CarOrder.TURN);
                return new Throttle(currentMaxSpeed);
            }
            
            Session.History.SetOrder(CarOrder.SPEED);
            //accelerating = false;
            return new Throttle(1.0);    
        }

        private bool _accelerating;
        private double _currentSp;

        private double GetBreakDistanceOld(double nextTurnMaxSpeed, double previousSpeed, double currentSpeed)
        {
            
            if (currentSpeed < double.Epsilon) return 0.0;
            var a = currentSpeed / 35;
            if (currentSpeed > 10) a = a * 0.55;
            return Math.Abs((nextTurnMaxSpeed * nextTurnMaxSpeed - currentSpeed * currentSpeed) / (2 * a));
        }

        private double GetMaxSpeedOld(Piece piece, int radius, double angle)
        {

            //var maxSpeed = Math.Abs(angle) > 70.0 ? Math.Sqrt(radius * t) : 10.0; // do testo
            var maxSpeed = Math.Sqrt(radius*t);// *0.9;


            var previousTurnDistance = Session.GetPreviousTurnDistance(piece);
            var previousTurnAngle = Session.GetPreviousTurnPiece(piece).angle;
            var breakingDistance = GetBreakDistanceOld(maxSpeed,Session.Previous1Speed, Session.CurrentSpeed);
            //var nextTurnDistance = Session.GetNextTurnDistance();
            var carAngle = Session.CurrentAngle;

            if (angle * previousTurnAngle < 0 && (previousTurnDistance < breakingDistance || (carAngle * angle < 0 && Math.Abs(carAngle) > 0.25)))
                maxSpeed *= 0.95;

            return maxSpeed;
        }
    }

}
