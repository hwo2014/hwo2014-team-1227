﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelloWorldOpen.Messages;

namespace HelloWorldOpen.Algorithms
{
    public class History
    {
        public History(IEnumerable<Piece> pieces)
        {
            foreach (var piece in pieces)
            {
                PiecesHistory[piece] = new PieceHistory();
            }
        }
        public Dictionary<Piece, PieceHistory> PiecesHistory = new Dictionary<Piece, PieceHistory>();
        public List<CarPositionsMessage> CarPositions = new List<CarPositionsMessage>();

        public void SetAngleHistory(Piece piece, double angle)
        {
            if (PiecesHistory[piece].maxAngle < Math.Abs(angle))
            {
                PiecesHistory[piece].maxAngle = Math.Abs(angle);
            }
        }

        public void SetCrash(Piece piece)
        {
            PiecesHistory[piece].crashed = true;
        }

        public void SetSpeedHistory(double currentSpeed)
        {
            CarPositions.Last().Speed = currentSpeed;
        }


        public void SetTargetSpeed(double targetSpeed)
        {
            CarPositions.Last().TargetSpeed = targetSpeed;
        }

        public void SetThrottle(double value)
        {
            CarPositions.Last().Throttle= value;
        }
    }
}