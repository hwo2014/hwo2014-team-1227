﻿using System;
using HelloWorldOpen.Commands;

namespace HelloWorldOpen.Algorithms
{
    public partial class MessageHandler
    {
        

        public SendMsg ConstantSpeedAlgorithm()
        {
            SendMsg msg = new Throttle(1.0);
            msg = EmergencyBreaking(msg);
            msg = NormalFlow(msg);
            msg = PushTurbo(msg);
            msg = MakeSwitch(msg);

            return msg;


            //if (Session.IsBreaking())
            //{
            //    if (Session.GetTargetSpeed() < 2 * Session.CurrentSpeed - Session.Previous1Speed) //still need to break
            //    {
            //        Session.History.SetOrder(CarOrder.BREAK);
            //        Session.History.SetTargetSpeed(Session.GetTargetSpeed());

            //        if (nextTurnDistance > 0.0 && nextTurnDistance > breakDistance) //breaking too fast
            //        {
            //            return new Throttle(breakDistance / nextTurnDistance * Session.GetTargetSpeed());
            //        }
                    
            //        return new Throttle(0.0); //breaking normally
            //    }
            //}

            //if (Session.CurrentSpeed > nextTurnMaxSpeed && breakDistance + Session.CurrentSpeed > nextTurnDistance)
            //{
            //    Session.History.SetOrder(CarOrder.BREAK);
            //    Session.History.SetTargetSpeed(nextTurnMaxSpeed);
            //    return new Throttle(0.0);
            //}

            //if (Session.IsTurn())
            //{
            //    //var currentTurnDistance = Session.GetCurrentTurnDistance(); // sprawdzic czy dobrze liczy 
            //    var currentTurnRadius = Session.GetCurrentTurnRadius();
            //    var currentTurnAngle = Session.GetCurrentTurnAngle();
            //    var currentMaxSpeed = GetMaxPieceSpeed(Session.CurrentPieceInfo, currentTurnRadius, currentTurnAngle);
                
            //    if (Session.CurrentSpeed - currentMaxSpeed > 0.1)
            //        return new Throttle(0.5);

            //    if (Math.Abs(Session.CurrentAngle) > 30.0 && Math.Abs(Session.CurrentAngle) < Math.Abs(Session.PreviousCarPosition.angle))
            //    {
            //        Session.History.SetOrder(CarOrder.SPEED);
            //        return new Throttle(1.0);
            //    }

            //    Session.History.SetOrder(CarOrder.TURN);
            //    return new Throttle(currentMaxSpeed);
            //}
            
            //Session.History.SetOrder(CarOrder.SPEED);
            //return new Throttle(1.0);    
        }

        public const int ROUNDS_TO_CRASH = 16;
        public const double CRASH_ANGLE = 60.0;
        private SendMsg EmergencyBreaking(SendMsg msg)
        {
            
            var previousDeltaAlfa1 = Session.CurrentAngle - Session.PreviousAngle1;
            var previousDeltaAlfa2 = Session.PreviousAngle1 - Session.PreviousAngle2;
            var previousDeltaAlfa3 = Session.PreviousAngle2 - Session.PreviousAngle3;
            var previousAcceleration1 = previousDeltaAlfa1 - previousDeltaAlfa2;
            var previousAcceleration2 = previousDeltaAlfa2 - previousDeltaAlfa3;
            var accelerationChange = previousAcceleration1 - previousAcceleration2;

            if (Math.Abs(Session.CurrentAngle + (previousDeltaAlfa1 + previousAcceleration1 + accelerationChange) * (ROUNDS_TO_CRASH + Session.Delay)) > CRASH_ANGLE)
                return new Throttle(0.0);
            return msg;
        }

        private SendMsg MakeSwitch(SendMsg msg)
        {
            return msg;
        }

        private SendMsg PushTurbo(SendMsg msg)
        {
            
            return msg;
        }

        private SendMsg NormalFlow(SendMsg msg)
        {
            var throttle = (Throttle) msg;
            var nextTurnMaxSpeed = GetMaxPieceSpeed(Session.NextTurnPiece);
            var breakDistance = Session.GetBreakDistance(Session.DelayedSpeed,nextTurnMaxSpeed);

            var maxAllowedSpeedBeforeNextTurn = Session.NextTurnDistance - Session.DelayedDistance - breakDistance;
            var maxAllowedSpeedCurrentPiece = GetMaxPieceSpeed(Session.CurrentPieceInfo);

            var targetSpeed = Math.Min(maxAllowedSpeedBeforeNextTurn, maxAllowedSpeedCurrentPiece);

            var targetThrottle = Session.GetTargetThrottle(Session.DelayedSpeed, targetSpeed);
         

            return new Throttle(Math.Min(throttle.value, targetThrottle));
        }

        private const double FRICTION = 0.38; //0.47;

        private double GetMaxPieceSpeed(Piece piece)
        {
            if (piece.radius == 0) return 100.0;
            if (piece.radius >= 180) return 9.0;

            //var maxSpeed = Math.Abs(angle) > 70.0 ? Math.Sqrt(radius * t) : 10.0; // do testo
            var maxSpeed = Math.Sqrt(piece.radius*FRICTION);// *0.9;


            //TODO: move to emergency break
            //var previousTurnDistance = Session.GetPreviousTurnDistance(piece);
            //var previousTurnAngle = Session.GetPreviousTurnPiece(piece).angle;
            //var breakingDistance = GetBreakDistance(maxSpeed);
            ////var nextTurnDistance = Session.GetNextTurnDistance();
            //var carAngle = Session.CurrentAngle;

            //if (piece.angle * previousTurnAngle < 0 && (previousTurnDistance < breakingDistance || (carAngle * piece.angle < 0 && Math.Abs(carAngle) > 0.25)))
            //    maxSpeed *= 0.95;

            return maxSpeed;
        }
    }

}
