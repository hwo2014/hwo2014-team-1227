﻿namespace HelloWorldOpen.Algorithms
{
    public class PieceHistory
    {
        public double maxAngle = 0.0;
        public double entrySpeedModificator = 1.0;
        public bool crashed = false;
    }

    public class TickHistory
    {
        public double Speed;
        public CarOrder CarOrder;
    }
}