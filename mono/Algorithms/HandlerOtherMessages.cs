﻿using System;
using System.Globalization;
using System.Linq;
using HelloWorldOpen.Commands;
using HelloWorldOpen.Messages;

namespace HelloWorldOpen.Algorithms
{
    public partial class MessageHandler
    {
        public Session Session = new Session();
        public MessageHandler(string carName, string carColor)
        {
            Session.SetCarInfo(carName,carColor);
        }

        public SendMsg HandleGameInit(GameInitMessage message)
        {
            Session.Game = message;
            var myCar = message.data.race.cars.First(x => x.id.name == Session.CarName);
            if (Session.CarColor == null)
            {
                
                Session.CarColor = myCar.id.color;
                
            }
            Session.History = new History(Session.Game.data.race.track.pieces);
            Console.WriteLine("Race init" + myCar.dimensions);
            return new Ping();
        }


        public SendMsg HandleYourCar(YourCarMessage deserializeObject)
        {
            return new Ping();
        }

        public SendMsg HandleGameEnd(GameEndMessage message)
        {
            var carBestLap = message.data.bestLaps.FirstOrDefault(x => x.car.color == Session.CarColor && x.car.name == Session.CarName);
            Console.WriteLine("RACE ENDED. BEST LAP: {0}", carBestLap == null ? "NO":carBestLap.result.millis.ToString(CultureInfo.InvariantCulture));
			return new Ping();
        }

        public void HandleTournamentEnd()
        {
            Console.WriteLine("TOURNAMENT END");
        }

        public SendMsg HandleCrash(CrashMessage deserializeObject)
        {
            Console.WriteLine("CRASH!!!");
            Session.History.SetCrash(Session.CurrentPieceInfo);
            return new Ping();
        }

        public SendMsg TurboEnd()
        {
            Console.WriteLine("TURBO END");    
            return new Ping();
        }

        public SendMsg HandleGameStart()
        {
            Console.WriteLine("RACE STARTS");
            return new Ping();
        }

        public SendMsg HandleJoinAcknowledge()
        {
            Console.WriteLine("CAR JOINED THE RACE");
            return new Ping();
        }

        public SendMsg HandleLapFinished()
        {
            //var lap = Session.GetPreviuosCarPosition().piecePosition.lap + 1;
            //for (var i = 0; i < Session.History.PiecesHistory.Values.Count; i++)
            //{
            //    var pieceHistory = Session.History.PiecesHistory.Values.ElementAt(i);
            //    var nextPieceHistory = Session.History.PiecesHistory.Values.ElementAt((i + 1) % Session.History.PiecesHistory.Values.Count);
            //    var nextAfterNextPieceHistory = Session.History.PiecesHistory.Values.ElementAt((i + 2) % Session.History.PiecesHistory.Values.Count);

            //    if (pieceHistory.crashed)
            //    {
            //        if (pieceHistory.entrySpeedModificator > 1.0)
            //            pieceHistory.entrySpeedModificator = 1.0;
            //        else
            //            pieceHistory.entrySpeedModificator /= 0.5;
            //        continue;
            //    }

            //    if (lap == 1 && pieceHistory.maxAngle < 33 && nextPieceHistory.maxAngle < 42 && nextAfterNextPieceHistory.maxAngle < 42)
            //    {
            //        pieceHistory.entrySpeedModificator *= (100 + (30 - pieceHistory.maxAngle) / 1.5) / 100;
            //    }

            //    if (lap == 2 && pieceHistory.maxAngle < 40 && nextPieceHistory.maxAngle < 45)
            //    {
            //        pieceHistory.entrySpeedModificator *= (100 + (40 - pieceHistory.maxAngle)/3) / 100;
            //    }

            //    //if (pieceHistory.maxAngle > 55)
            //    //{
            //    //    pieceHistory.entrySpeedModificator /= 1.05;
            //    //}

            //    //Console.WriteLine("Piece {0} : {1} , {2}", i, pieceHistory.entrySpeedModificator, pieceHistory.maxAngle);
            //}
            return new Ping();
        }

        public SendMsg HandleTurboAvailable(TurboAvailableMessage turboAvailableMessage)
        {
            Console.WriteLine("TURBO::::: " + turboAvailableMessage.data.turboFactor);
            Session.Turbo = turboAvailableMessage.data;
            return new Ping();
        }
    }
}
