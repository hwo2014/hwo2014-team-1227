﻿using System.Collections.Generic;
using System.Linq;

namespace HelloWorldOpen.Algorithms
{
    public class Chromosone
    {
        public Chromosone(double turnBreakIndicator, double deltaAlfaBreakIndicator, Dictionary<int,double> turnEntrySpeedIndicator)
        {
            TurnBreakIndicator = turnBreakIndicator;
            DeltaAlfaBreakIndicator = deltaAlfaBreakIndicator;
            _turnEntrySpeedIndicator = turnEntrySpeedIndicator;
        }
        public double TurnBreakIndicator; //d = (dV * X)   // odl. w jakiej nalezy zaczac hamowac    np. 10
        

        public double GetTurnEntrySpeedIndicator(int r)
        {

            var higher = _turnEntrySpeedIndicator.Keys.Where(x => x > r).Min();
            var lower = _turnEntrySpeedIndicator.Keys.Where(x => x <= r).Max();

            var higherSpeed = _turnEntrySpeedIndicator[higher];
            var lowerSpeed = _turnEntrySpeedIndicator[lower];


            return (double)(r - lower)/(higher - lower)*(higherSpeed - lowerSpeed) + lowerSpeed;

            //return r > 75 ? 0.075 : 0.06;
        }//V = X*r    , np. 0.1
        public double DeltaAlfaBreakIndicator; //kiedy wychylenie zbyt szybko postepuje np. 1
        private readonly Dictionary<int, double> _turnEntrySpeedIndicator;
    }
}