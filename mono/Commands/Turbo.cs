﻿using System;

namespace HelloWorldOpen.Commands
{
    public class Turbo : SendMsg
    {
        public string value;

        public Turbo(string value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "turbo";
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}