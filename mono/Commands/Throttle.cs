﻿using System;

namespace HelloWorldOpen.Commands
{
    public class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
            //> 1.0
            //                 ? value/10.0
            //                 : value;
            //if (value > 1.0) value = 1.0;
            //if (value < 0.0) value = 0.0;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }

        public override string ToString()
        {
            return string.Format("{0}",value);
        }
    }
}