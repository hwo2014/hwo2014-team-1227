﻿namespace HelloWorldOpen.Commands
{
    public class JoinRace : SendMsg
    {
        public BotId botId;
        public int carCount;
        public string trackName;

        public JoinRace(string name, string key, string trackName, int carCount)
        {
            botId = new BotId
                {
                    name = name,
                    key = key      
                };
            this.trackName = trackName;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    public class BotId
    {
        public string name;
        public string key;
    }
}
